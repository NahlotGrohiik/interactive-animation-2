﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTD_Code : MonoBehaviour
{
    [SerializeField] float _speed;
    [SerializeField] float _turnspeed;
    [SerializeField] Rigidbody _rb;
    [SerializeField] Animator _anim;
    [SerializeField] GameObject _camera;
    private Vector2 _input;
    private Vector3 _forward;
    private Vector3 _right;
    void Update ()
    {
        _input.x = Input.GetAxis ("Horizontal");
        _input.y = Input.GetAxis ("Vertical");
        _forward = _camera.transform.forward;
        _right = _camera.transform.right;
        _forward.y = 0f;
        _right.y = 0f;
        _forward.Normalize ();
        _right.Normalize ();
    }
    private void FixedUpdate ()
    {
        if (_input != Vector2.zero)
        {
            Vector3 desiredMoveDirection = _forward * _input.y + _right * _input.x;
            _rb.velocity = desiredMoveDirection * _speed * Time.fixedDeltaTime;
            transform.rotation = Quaternion.RotateTowards (transform.rotation, Quaternion.LookRotation (new Vector3 (_rb.velocity.x, 0, _rb.velocity.z)), _turnspeed * Time.fixedDeltaTime); // rotate player in direction of velocity
        }
        _anim.SetFloat ("x", _input.x); // update animation
        _anim.SetFloat ("z", _input.y); // update animation
    }
}